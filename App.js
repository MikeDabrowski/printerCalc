/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

type Props = {};
type State = {
  hours: number,
  minutes: number,
  percent: number,
  remainingMinutes: number,
  remainingTime: number
}
export default class App extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      hours: 0,
      minutes: 0,
      percent: 0,
      remainingMinutes: 0,
      remainingTime: 0
    };
    this.updateMinutes = this.updateMinutes.bind(this);
    this.updateHours = this.updateHours.bind(this);
    this.updatePercentages = this.updatePercentages.bind(this);
    this.calcTimeRem = this.calcTimeRem.bind(this);
  }

  updateMinutes(event) {
    this.setState({minutes: +event.nativeEvent.text}, this.calcTimeRem)
  }

  updateHours(event) {
    this.setState({hours: +event.nativeEvent.text}, this.calcTimeRem)
  }

  updatePercentages(event) {
    this.setState({percent: +event.nativeEvent.text}, this.calcTimeRem)
  }

  calcTimeRem() {
    const remainingMinutes = (100 * (this.state.hours * 60 + this.state.minutes) / (this.state.percent)) || 0;
    const remainingTime = `${remainingMinutes / 60}h : ${remainingMinutes % 60}m`;
    this.setState({remainingMinutes, remainingTime})
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={{height: 40, width: 100}}
          placeholder="hours"
          keyboardType='numeric'
          onChange={this.updateHours}
        />
        <TextInput
          style={{height: 40, width: 100}}
          placeholder="minutes"
          keyboardType='numeric'
          onChange={this.updateMinutes}
        />
        <TextInput
          style={{height: 40, width: 100}}
          placeholder="Percentage"
          keyboardType='numeric'
          onChange={this.updatePercentages}
        />
        <Text style={styles.welcome}>
          Time in minutes: {this.state.hours * 60 + this.state.minutes}
        </Text>
        <Text style={styles.welcome}>
          Percent: {this.state.percent}
        </Text>
        <Text style={styles.welcome}>
          Remaining minutes: {this.state.remainingMinutes}m
        </Text>
        <Text style={styles.welcome}>
          {this.state.remainingTime}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
